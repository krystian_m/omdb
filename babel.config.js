const { devProdTestReducer } = require('config-reducer')

module.exports = api =>
  devProdTestReducer(
    {
      presets: ['@babel/preset-env', '@babel/preset-react'],
      plugins: [
        [
          'emotion', // first
          {
            hoist: true,
            development: {
              sourceMap: true,
              autoLabel: true,
              hoist: false,
            },
          },
        ],

        // Stage 2
        ['@babel/plugin-proposal-decorators', { legacy: true }],
        '@babel/plugin-proposal-function-sent',
        '@babel/plugin-proposal-export-namespace-from',
        '@babel/plugin-proposal-numeric-separator',
        '@babel/plugin-proposal-throw-expressions',

        // Stage 3
        '@babel/plugin-syntax-dynamic-import',
        '@babel/plugin-syntax-import-meta',
        ['@babel/plugin-proposal-class-properties', { loose: false }],
        '@babel/plugin-proposal-json-strings',

        'react-hot-loader/babel',
        { development: ['@babel/plugin-transform-react-jsx-source'] },
        {
          production: [
            'dev-expression', // ---------------- >  unnecessary ?
            'transform-react-remove-prop-types',
          ],
        },
      ],
    },
    api.env()
  )
