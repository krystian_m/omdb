## install/setup
```
git clone git@gitlab.com:krystian_m/omdb.git
cd imdb
cp .env.example .env
yarn
```

## run

- development

```
yarn dev
```

- production

```
yarn prod
yarn start
```

## open

http://localhost:3030
