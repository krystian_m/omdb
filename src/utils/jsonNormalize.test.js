import jsonNormalize from './jsonNormalize'

it('shoud convert key to camelCase', () => {
  expect(jsonNormalize({ FooBar: 'a' })).toEqual({ fooBar: 'a' })
})

it('shoud remove N/A', () => {
  expect(jsonNormalize({ Foo: 'N/A' })).toEqual({})
})

it('shoud remove NOT RATED', () => {
  expect(jsonNormalize({ Foo: 'NOT RATED' })).toEqual({})
})

it('shoud convert numbers', () => {
  expect(jsonNormalize({ a: '2', b: '2a' })).toEqual({ a: 2, b: '2a' })
})

it('shoud convert booleans', () => {
  expect(jsonNormalize({ Response: 'False' })).toEqual({ response: false })
})

it('shoud work recursively', () => {
  expect(jsonNormalize({ Foo: [{ Bar: 'a' }] })).toEqual({
    foo: [{ bar: 'a' }],
  })
})
