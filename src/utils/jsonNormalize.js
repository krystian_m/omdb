const camelCase = str => str[0].toLowerCase() + str.slice(1)

const transformNumber = str =>
  Number.isNaN(Number(str)) ? str : parseInt(str, 10)

const convertField = (key, value) =>
  key === 'Response'
    ? { response: value === 'True' }
    : { [camelCase(key)]: transformNumber(value) }

const filterUnused = (key, value) =>
  ['N/A', 'NOT RATED'].includes(value) ? {} : convertField(key, value)

const normalize = obj =>
  Object.entries(obj).reduce(
    (accu, [key, value]) => ({
      ...accu,
      ...(Array.isArray(value)
        ? { [camelCase(key)]: value.map(x => normalize(x)) }
        : filterUnused(key, value)),
    }),
    {}
  )

export default json => normalize(json)
