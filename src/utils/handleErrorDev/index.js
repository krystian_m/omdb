const path = require('path')
const Anser = require('anser')
const PrettyError = require('pretty-error')
// import ErrorStackParser from 'error-stack-parser'

const pe = new PrettyError()
pe.appendStyle({
  'pretty-error': {
    display: 'block',
    marginLeft: '0',
    marginTop: '1',
  },
  'pretty-error > header ': {
    display: 'block',
  },
  'pretty-error > header > title > kind': {
    color: 'white',
    padding: '0 0', // top/bottom left/right
  },

  // the 'colon' after 'Error':
  'pretty-error > header > colon': {
    display: 'none',
  },

  // our error message
  'pretty-error > header > message': {
    display: 'block',
    marginTop: '1',
    marginBottom: '1',
    padding: '0 1', // top/bottom left/right
  },
})

const page = ({ status, name, message, file, line }) => `<!doctype html>
  <html>
    <head>
        <title>Error</title>
        <link rel="stylesheet" type="text/css" href="https://cdn.rawgit.com/floatdrop/express-stackman/ master/css/index.css">
        <style>
          body {
            font-family: Arial, Helvetica, sans-serif;
            font-size: 14px;
            margin: 0;
          }
          h1 {
            font-size: 36px;

          }
          .code {
            margin-top: 32px;
            margin-bottom: 32px;
            font-size: 14px;
            background: #efefef;
          }
          .gutter {
            padding-left: 4%;
            padding-right: 4%;
          }
        </style>
    </head>
    <body>
      <div class="gutter">
        <h1>Error</h1>
        <h2><i>${status}</i> — ${name}</h2>
        <div>File: ${file}</div>
        <div>Line: ${line}</div>
      </div>
      <h3>${message}</h3>
      <ul id="stacktrace">
      </ul>
    </body>
    <script src="https://google-code-prettify.googlecode.com/svn/loader/run_prettify.js"></script>
    <link rel="stylesheet" type="text/css" href="https://cdn.rawgit.com/floatdrop/express-stackman/master/css/prettify.css">
  </html>
`
// ${stack.map}
//     <li>{{filename}}:{{line}}:{{column}}</li>
//     {{{context}}}
// {{/frames}}

// eslint-disable-next-line no-unused-vars
module.exports = () => (err, req, res, next) => {
  let message = Anser.ansiToHtml(err.message.replace(/ /g, '_@SPACE@_'))
    .replace(/\n/g, '<br>')
    .replace(/_@SPACE@_/g, '&nbsp;')

  message = `<div class="code gutter" style="font-family: monospace;">${message}</div>`

  // console.log('>>ERROR>> ', Object.keys(err))

  const file =
    err &&
    err.error &&
    err.error.error &&
    path.relative(__dirname, err.error.error.message.match(/(.*?):/)[1])

  const data = {
    status: err.status || '',
    file,
    name:
      (err && err.error && err.error.error && err.error.error.name) || err.name,
    message,
    line:
      (err && err.error && err.error.error && err.error.error.loc.line) ||
      (err.locations && err.locations[0].line),
    column: err && err.error && err.error.error && err.error.error.loc.column,
  }

  console.error(pe.render(err))
  res.send(page(data))
}
