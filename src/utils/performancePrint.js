let performance =
  ';if (!window||!window.performance){return;};var pe=performance.timing;'
performance += [
  { name: 'DNS lookup', action: 'pe.domainLookupEnd - pe.domainLookupStart' },
  { name: 'Response time', action: 'pe.responseEnd - pe.requestStart' },
  { name: 'Time to Interactive', action: 'pe.domInteractive - pe.domLoading' },
  { name: 'Page render time', action: 'pe.domComplete - pe.domLoading' },
  { name: 'Page load', action: 'pe.loadEventEnd - pe.navigationStart' },
  { name: '= (resp-tti) ', action: 'pe.domInteractive - pe.responseEnd' },
]
  .map(x => `console.info('[STAT] ${x.name} ',${x.action});`)
  .join('')
const performancePrint = `setTimeout(function(){${performance}}, 50);`

export default performancePrint
