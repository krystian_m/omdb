export default beginYear =>
  // eslint-disable-next-line prettier/prettier, prefer-spread
  Array.apply(null, Array((new Date()).getFullYear() - (beginYear - 1) ))
    .map((_, i) => i + beginYear)
    .reverse()
