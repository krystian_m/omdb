import yearsArray from './yearsArray'

it('shoud return years', () => {
  expect(yearsArray(2017)).toEqual([2019, 2018, 2017])
})
