const express = require('express')
const warning = require('warning')
const middlewares = require('./middlewares')
const paths = require('../../config/paths')
// eslint-disable-next-line import/no-unresolved
const clientStats = require('../../build/stats.json')
// eslint-disable-next-line import/no-unresolved
const serverRender = require('../../build/ssr').default

const { SERVER_HOST, SERVER_PORT } = process.env

/* eslint-disable prettier/prettier */

warning(
  clientStats && serverRender,
  '\n\n'+
  '******************************************************\n'+
  '*  Can not load /build/stats.json or /build/ssr.js   *\n'+
  '******************************************************\n'+
  '\n'
)

warning(
  paths.url.public.path &&
  SERVER_HOST &&
  SERVER_PORT,
  '\n\n'+
  '***********************************************\n'+
  '*  Empty PUBLIC_PATH or (other enviroments)  *\n'+
  '*  If you run server without a webpack,       *\n'+
  '*  set the ENVs, or add dotenv module.        *\n'+
  '***********************************************\n'+
  '\n'
)

const server = express()

middlewares(server)

server.use(serverRender({ clientStats }))

server.listen(SERVER_PORT, () => {
  console.info(
    `Listening @ ${SERVER_HOST}:${SERVER_PORT}`
  )
})
