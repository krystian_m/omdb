require('colors')
require('dotenv-safe').config()
require('pretty-error').start()
const ip = require('ip')
const express = require('express')
const webpack = require('webpack')
const webpackDevMiddleware = require('webpack-dev-middleware')
const webpackHotMiddleware = require('webpack-hot-middleware')
const webpackHotServerMiddleware = require('webpack-hot-server-middleware')
const errorOverlayMiddleware = require('react-dev-utils/errorOverlayMiddleware')
const clientConfig = require('../../config/webpack.client')
const serverConfig = require('../../config/webpack.ssr')
const middlewares = require('./middlewares')
const config = require('../../config')
// const handleError = require('../utils/handleError')

const { publicPath } = clientConfig.output
const { SERVER_PORT } = process.env
const server = express()
let isBuilt = false

middlewares(server)

const done = () =>
  !isBuilt &&
  server.listen(SERVER_PORT, () => {
    isBuilt = true
    const { info } = console
    info('-------------------------------------'.cyan)
    info(`   Local: http://localhost:${SERVER_PORT}/`)
    info(`  Global: http://${ip.address()}:${SERVER_PORT}/`)
    info('-------------------------------------'.cyan)
    if (config.ANALYZE) {
      info(' Boundle: http://localhost:8888/')
    }
    info('   Ratel: http://localhost:8000/')
    info(` Wallaby: http://${ip.address()}:51245/`)
    info('-------------------------------------'.cyan)
  })

const compiler = webpack([clientConfig, serverConfig])
const clientCompiler = compiler.compilers.find(x => x.name === 'client')
const devMiddleware = webpackDevMiddleware(
  compiler,
  config.devMiddleware(publicPath)
)

server.use(devMiddleware)
server.use(webpackHotMiddleware(clientCompiler))
server.use(webpackHotServerMiddleware(compiler))
server.use(errorOverlayMiddleware())
// server.use(handleError())

devMiddleware.waitUntilValid(done)
