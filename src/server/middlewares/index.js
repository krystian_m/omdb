const pathNode = require('path')
const helmet = require('helmet')
const express = require('express')
const favicon = require('serve-favicon')
const staticGzip = require('express-static-gzip')
const cacheController = require('express-cache-controller')
const config = require('../../../config')
const paths = require('../../../config/paths')

module.exports = server => {
  server.use(helmet())

  if (config.PROD) {
    server.use(
      paths.url.public,
      cacheController({
        maxAge: 3600 * 24 * 365 * 3, // 3y in seconds
        noTransform: true,
        immutable: true,
        public: true,
      }),
      staticGzip(paths.dir.build.public, {
        enableBrotli: true,
      })
    )
    server.use('/', express.static(paths.dir.static))
    server.use(paths.url.public, express.static(paths.dir.public))
  } else {
    server.use(paths.url.public, express.static(paths.dir.dll.path))
    server.use(paths.url.public, express.static(paths.dir.test.files))
  }

  server.use(favicon(pathNode.join(paths.dir.static, 'favicon.ico')))
}
