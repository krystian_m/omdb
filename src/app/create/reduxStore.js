/* eslint-disable global-require */

// import { combineReducers } from 'redux-immutable'
import { createStore, applyMiddleware, combineReducers } from 'redux'
import { reducer as form } from 'redux-form'
// import { reducer as form } from 'redux-form/immutable'
import { composeWithDevTools } from 'redux-devtools-extension'
import { combineEpics, createEpicMiddleware } from 'redux-observable'
// import thunk from 'redux-thunk'
// import { fromJS } from 'immutable'
import { reducers, epics } from 'store'

export const rootReducer = combineReducers({
  ...reducers,
  form,
})

export const rootEpic = combineEpics(...epics)

const epicMiddleware = createEpicMiddleware()

export default initialState => {
  const store = createStore(
    rootReducer,
    initialState, // fromJS(initialState),
    composeWithDevTools(
      applyMiddleware(epicMiddleware)
      // applyMiddleware(thunk)
    )
  )

  epicMiddleware.run(rootEpic)

  if (module.hot && _DEV_) {
    module.hot.accept('../store', () => {
      const nextReducers = require('../store/index.js').reducers
      const nextRootReducer = combineReducers({
        form,
        ...nextReducers,
      })
      store.replaceReducer(nextRootReducer)
    })
  }
  return store
}
