import React from 'react'
import ReactDOM from 'react-dom'
import { AppContainer as HotLoader } from 'react-hot-loader'
import { Provider as StoreProvider } from 'react-redux'
import { BrowserRouter } from 'react-router-dom'
import recursiveUnescape from 'recursive-unescape'
import createReduxStore from 'create/reduxStore'
import { hydrate } from 'emotion'
import App from 'components/App'
import ScrollRestoration from '../utils/scroll-restoration'

const debug = require('debug')('client')

debug('- start')
const rootElement = document.getElementById('root')
const initialData = recursiveUnescape(window.App)
delete window.App

hydrate(initialData.emotionIDS)
const store = createReduxStore(initialData.reduxState)
debug('get #root and create: store,client')

const render = Component =>
  ReactDOM.hydrate(
    <HotLoader>
      <BrowserRouter>
        <ScrollRestoration>
          <StoreProvider store={store}>
            <Component />
          </StoreProvider>
        </ScrollRestoration>
      </BrowserRouter>
    </HotLoader>,
    rootElement
  )
debug('Component')

render(App)
debug('render')

if (_DEV_ && module.hot) {
  debug('hot... ')
  module.hot.accept('./components/App', () => {
    // eslint-disable-next-line global-require
    const UpdatedApp = require('./components/App').default
    render(UpdatedApp)
  })
  debug('hot... ok')
} else {
  debug('hot... DISABLED')
}
