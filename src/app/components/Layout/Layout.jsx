import React from 'react'
import { Route, Switch } from 'react-router-dom'
import { css } from 'emotion'
import Header from 'components/Header'
import Search from 'components/Search'
import Detail from 'components/Detail'

const layoutCSS = css`
  height: 100%;
  display: flex;
  flex-direction: column;
  padding-left: 7vw;
  padding-right: 7vw;
  padding-top: 24px;
`

const Layout = () => (
  <div className={layoutCSS}>
    <Header />
    <main>
      <Switch>
        <Route exact path="/" component={Search} />
        <Route path="/(.+)" component={Detail} />
      </Switch>
    </main>
  </div>
)

export default Layout
