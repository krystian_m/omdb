import React from 'react'
import Caption from './index'

it('should render default', () => {
  const wrapper = renderer.create(<Caption />)
  expect(wrapper.toJSON()).toMatchSnapshot()
})

it('should render with string details', () => {
  const wrapper = renderer.create(<Caption details="foo" />)
  expect(wrapper.toJSON()).toMatchSnapshot()
})

it('should render with srray details', () => {
  const wrapper = renderer.create(<Caption details={['foo', 'bar']} />)
  expect(wrapper.toJSON()).toMatchSnapshot()
})
