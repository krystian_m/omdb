import React from 'react'
import PropTypes from 'prop-types'
import { css } from 'emotion'

const detailsCSS = css`
  color: #a7a7a7;
  font-size: 0.875rem;
  margin-right: 16px;
`
const Caption = ({ details }) =>
  details && (
    <div>
      {Array.isArray(details) ? (
        details.filter(x => !!x).map(x => (
          <span key={`desc${x}`} className={detailsCSS}>
            {x}
          </span>
        ))
      ) : (
        <div className={detailsCSS}>{details}</div>
      )}
    </div>
  )

export default Caption

Caption.propTypes = {
  details: PropTypes.oneOfType([PropTypes.array, PropTypes.node]),
}

Caption.defaultProps = {
  details: null,
}
