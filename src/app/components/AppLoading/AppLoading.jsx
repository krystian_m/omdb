/* eslint-disable react/destructuring-assignment */
import React from 'react'
import PropTypes from 'prop-types'
import { css } from 'emotion'
import styled, { keyframes } from 'react-emotion'

const appLoadingCSS = css`
  position: fixed;
  top: 0;
  left: 0;
  z-index: 10000001;
  width: 100%;
`

const progress = keyframes`
  0% {
    width: 5%;
  }
  10% {
    width: 20%;
  }
  20% {
    width: 50%;
  }
  50% {
    width: 80%;
  }
  100% {
    width: 90%;
  }
`

const showLoading = css`
  width: 90%;
  opacity: 1;
  transition: opacity 0s;
  animation: ${progress} 10s alternate ease;
`

const ProgressBar = styled('div')`
  height: 4px;
  width: 100%;
  background-color: gray;
  transition: opacity 0.6s 0.2s;
  opacity: 0;
`

class AppLoading extends React.PureComponent {
  static propTypes = {
    isLoading: PropTypes.bool,
  }

  static defaultProps = {
    isLoading: true,
  }

  constructor(props) {
    super(props)
    this.state = {
      isLoading: true,
    }
  }

  componentDidMount() {
    if (this.props.isLoading !== this.state.isLoading) {
      // eslint-disable-next-line react/no-did-mount-set-state
      this.setState({
        isLoading: this.props.isLoading,
      })
    }
  }

  render() {
    const { isLoading } = this.state
    return (
      <div className={appLoadingCSS}>
        <ProgressBar className={isLoading && showLoading} />
      </div>
    )
  }
}

export default AppLoading
