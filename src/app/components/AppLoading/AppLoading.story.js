/* eslint-disable no-console */
import React from 'react'
import { storiesOf } from '@storybook/react'
import AppLoading from './index'

storiesOf('AppLoading', module).add('Default', () => (
  <AppLoading isLoading style={{ width: '100%' }} />
))
