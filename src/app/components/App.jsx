import React from 'react'
import AppLoading from 'components/AppLoading'
import Layout from 'components/Layout'
import { Helmet } from 'react-helmet'
import { injectGlobal, css } from 'emotion'

injectGlobal`
  *,
  *:before,
  *:after {
    box-sizing: border-box;
  }

  html {
    display: table;
    height: 100%;
    width: 100%;
    margin: 0;
    padding: 0;
  }

  body {
    display:table-cell;
    width:100%;
    margin: 0;
    padding: 0;
    font-weight: sans-serif;
    font-family: sans-serif;
    text-rendering: optimizeLegibility;
    /* Note: DISABLE FOR LONG PAGES */
    font-smoothing: antialiased;
  }

  #root {
    width: 100%;
    height: 100%;
  }
`

const appCSS = css`
  height: 100%;
  overflow: hidden;
`

const App = () => (
  <div className={appCSS}>
    <Helmet
      titleTemplate="%s | OMDb"
      title="movies"
      titleAttributes={{ lang: 'pl' }}
      meta={[
        { name: 'author', content: 'Krystian Mazur' },
        { name: 'description', content: 'OMDb' },
        {
          name: 'viewport',
          content: 'width=device-width, initial-scale=1',
        },
      ]}
    />
    <AppLoading isLoading={false} />
    <Layout />
  </div>
)

export default App
