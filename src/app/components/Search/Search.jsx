import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { css } from 'emotion'
import SearchForm from 'components/SearchForm'
import Movies from 'components/Movies'
import Pagination from 'components/Pagination'
import Error from 'components/Error'

const formCSS = css`
  margin-bottom: 56px;
`

const Search = ({ movies, totalResults, loading, error }) => (
  <div>
    <SearchForm className={formCSS} />
    <Error text={error} />
    <Movies items={movies} loading={loading} />
    <Pagination total={totalResults} />
  </div>
)

const mapStateToProps = ({ movies }) => ({
  movies: movies.searchResult.search,
  totalResults: movies.searchResult.totalResults,
  loading: movies.loading,
  error: movies.error,
})

export default connect(mapStateToProps)(Search)

Search.propTypes = {
  movies: PropTypes.arrayOf(PropTypes.object),
  totalResults: PropTypes.number,
  loading: PropTypes.bool,
  error: PropTypes.string,
}

Search.defaultProps = {
  movies: [],
  totalResults: 0,
  loading: false,
  error: '',
}
