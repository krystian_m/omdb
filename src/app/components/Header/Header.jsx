import React from 'react'
import { css } from 'emotion'

const headerCSS = css`
  width: 100%;
  h1 {
    font-weight: 300;
    font-size: 2rem;
    color: #e0e0e0;
  }
`

const Header = () => (
  <header className={headerCSS}>
    <h1>Movies Database</h1>
  </header>
)

export default Header
