import React from 'react'
import { MemoryRouter } from 'react-router'
import DetailFooter from './index'

const withRouter = child => (
  <MemoryRouter initialEntries={['/']}>{child}</MemoryRouter>
)

it('should render default', () => {
  const wrapper = renderer.create(withRouter(<DetailFooter />))
  expect(wrapper.toJSON()).toMatchSnapshot()
})

it('should render with website', () => {
  const wrapper = renderer.create(
    withRouter(<DetailFooter website="foo.htm" />)
  )
  expect(wrapper.toJSON()).toMatchSnapshot()
})
