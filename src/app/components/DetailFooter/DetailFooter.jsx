import React from 'react'
import PropTypes from 'prop-types'
import { Route } from 'react-router-dom'
import { css } from 'emotion'
import Button from 'components/Button'

const buttonsCSS = css`
  display: flex;
  justify-content: space-between;
  margin-top: 56px;
  margin-bottom: 56px;
  @media (min-width: 768px) {
    justify-content: flex-start;
  }
`
const buttonCSS = css`
  @media (min-width: 768px) {
    margin-right: 32px;
  }
`

export const DetailFooter = ({ goBack, website }) => (
  <nav className={buttonsCSS}>
    <Button className={buttonCSS} type="div" onClick={goBack}>
      Go back
    </Button>

    {!!website && (
      <Button className={buttonCSS} type="a" href={website}>
        Web site
      </Button>
    )}
  </nav>
)

export default props => (
  <Route>
    {({ history }) => <DetailFooter goBack={history.goBack} {...props} />}
  </Route>
)

DetailFooter.propTypes = {
  goBack: PropTypes.func.isRequired,
  website: PropTypes.string,
}

DetailFooter.defaultProps = {
  website: '',
}
