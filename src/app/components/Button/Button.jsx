import React from 'react'
import PropTypes from 'prop-types'
import { css, cx } from 'emotion'

const buttonCSS = css`
  padding: 8px 24px;
  white-space: nowrap;
  line-height: 1rem;
  display: inline-flex;
  align-items: center;
  justify-content: center;
  font-family: sans-serif;
  font-size: 0.8125rem;
  font-weight: 500;
  text-align: center;
  letter-spacing: 0.02rem;
  overflow: hidden;
  text-decoration: none;
  box-shadow: 0 1px 2px 0px #7b7b7b;
  border-radius: 2px;
  background-color: hsl(0, 0%, 99%);
  color: hsl(0, 0%, 40%);
  &.focus,
  &:focus {
    outline: 0;
    box-shadow: 0 0 20px #e2e2e2;
  }
  &.hover,
  &:hover {
    background-color: hsl(0, 0%, 97%);
  }
  &.active:not(.disabled),
  &:active:not(.disabled) {
    box-shadow: inset 0 0px 2px 0px #7b7b7b;
  }
  &.disabled {
    color: hsl(0, 0%, 60%);
    box-shadow: 0 1px 1px 0px #7b7b7b;
    cursor: not-allowed;
  }
`

const handleEnterKey = e => {
  if (e.key === 'Enter' || e.key === ' ') {
    e.preventDefault()
    e.target.click()
  }
}

const refactorProps = (
  type,
  { className, disabled, onClick, role, tabIndex, ...props }
) => ({
  role,
  tabIndex,
  className: cx(className, buttonCSS),
  ...(type !== 'button' && !disabled
    ? {
        onKeyPress: handleEnterKey,
        onClick,
        ...props,
      }
    : {
        disabled,
      }),
})

const Button = ({ type, children, ...props }) =>
  React.createElement(type, refactorProps(type, props), children)

export default Button

Button.propTypes = {
  type: PropTypes.string,
  children: PropTypes.node,
  className: PropTypes.string,
  disabled: PropTypes.bool,
  onClick: PropTypes.func,
  role: PropTypes.string,
  tabIndex: PropTypes.number,
}

Button.defaultProps = {
  type: 'button',
  children: null,
  className: '',
  disabled: false,
  onClick: () => {},
  role: 'button',
  tabIndex: 0,
}
