import React from 'react'
import Button from './index'

it('should render default', () => {
  const wrapper = renderer.create(<Button>Name</Button>)
  expect(wrapper.toJSON()).toMatchSnapshot()
})

it('should render as div', () => {
  const wrapper = renderer.create(<Button type="div">Name</Button>)
  expect(wrapper.toJSON()).toMatchSnapshot()
})

it('should set role', () => {
  const wrapper = renderer.create(<Button role="alert">Name</Button>)
  expect(wrapper.toJSON()).toMatchSnapshot()
})

it('should set tabIndex', () => {
  const wrapper = renderer.create(<Button tabIndex={-1}>Name</Button>)
  expect(wrapper.toJSON()).toMatchSnapshot()
})

it('should set className', () => {
  const wrapper = renderer.create(<Button className="foo">Name</Button>)
  expect(wrapper.toJSON()).toMatchSnapshot()
})
