import React from 'react'
import Loading from './index'

it('should render default', () => {
  const wrapper = renderer.create(<Loading />)
  expect(wrapper.toJSON()).toMatchSnapshot()
})

it('should render with show', () => {
  const wrapper = renderer.create(<Loading show />)
  expect(wrapper.toJSON()).toMatchSnapshot()
})
