import React from 'react'
import PropTypes from 'prop-types'
import { RingLoader } from 'react-spinners'
import { css } from 'emotion'

const loadingCSS = css`
  position: absolute;
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: 55;
  pointer-events: none;
  background-color: #ffffffe6;
  margin: -5px; /* Movie box-shadow fix */
`
const centerCSS = css`
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
`

const Loading = ({ show }) =>
  show && (
    <div className={loadingCSS}>
      <div className={centerCSS}>
        <RingLoader sizeUnit="px"size={96} color="#e0e0e0" loading={show} />
      </div>
    </div>
  )

export default Loading

Loading.propTypes = {
  show: PropTypes.bool,
}

Loading.defaultProps = {
  show: false,
}
