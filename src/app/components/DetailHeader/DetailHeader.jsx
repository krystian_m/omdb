import React from 'react'
import PropTypes from 'prop-types'
import { css } from 'emotion'
import Caption from 'components/Caption'

const titleCSS = css`
  font-size: 2rem;
  margin-top: 0;
  margin-bottom: 16px;
`

export const DetailHeader = ({ title, details }) =>
  !!title && (
    <header>
      <h1 className={titleCSS}>{title}</h1>
      <Caption details={details} />
    </header>
  )

export default DetailHeader

DetailHeader.propTypes = {
  title: PropTypes.string,
  details: PropTypes.oneOfType([PropTypes.string, PropTypes.array]),
}

DetailHeader.defaultProps = {
  title: '',
  details: '',
}
