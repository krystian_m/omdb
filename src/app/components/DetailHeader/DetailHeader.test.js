import React from 'react'
import DetailHeader from './index'

it('should render default', () => {
  const wrapper = renderer.create(<DetailHeader />)
  expect(wrapper.toJSON()).toMatchSnapshot()
})

it('should render with title', () => {
  const wrapper = renderer.create(<DetailHeader title="Foo" />)
  expect(wrapper.toJSON()).toMatchSnapshot()
})

it('should render with details as string', () => {
  const wrapper = renderer.create(<DetailHeader title="Foo" details="bar" />)
  expect(wrapper.toJSON()).toMatchSnapshot()
})

it('should render with details as array', () => {
  const wrapper = renderer.create(
    <DetailHeader title="Foo" details={['bar', 'buzz']} />
  )
  expect(wrapper.toJSON()).toMatchSnapshot()
})
