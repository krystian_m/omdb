import React from 'react'
import PropTypes from 'prop-types'
import { Field, reduxForm } from 'redux-form'
import { css } from 'emotion'
import yearsArray from '../../../utils/yearsArray'

const textCSS = css`
  font-size: 2rem;
  outline: 0;
  border: 0;
  padding: 0;
  width: 100%;
  line-height: 1.5;
  background: transparent;
  margin-bottom: 16px;
  background-color: #f9f9f9;
`
const selectCSS = css`
  margin-right: 16px;
`

const Types = () =>
  ['Movie', 'Series', 'Episode'].map(type => (
    <option key={type} value={type.toLowerCase()}>
      {type}
    </option>
  ))

const Years = () =>
  yearsArray(1900).map(year => (
    <option key={year} value={year}>
      {year}
    </option>
  ))

const SearchForm = ({ handleSubmit, className }) => (
  <form onSubmit={handleSubmit} className={className}>
    <Field
      name="search"
      component="input"
      className={textCSS}
      placeholder="Search title.."
      type="text"
    />

    <Field name="type" component="select" className={selectCSS}>
      <option value="">Type</option>
      <Types />
    </Field>

    <Field name="year" component="select" className={selectCSS}>
      <option value="">Year</option>
      <Years />
    </Field>
  </form>
)

export default reduxForm({
  form: 'search',
  onSubmit: () => {},
  destroyOnUnmount: false,
})(SearchForm)

SearchForm.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  className: PropTypes.string,
}

SearchForm.defaultProps = {
  className: '',
}
