import React from 'react'
import PropTypes from 'prop-types'
import ReactPagination from 'rc-pagination'
import { connect } from 'react-redux'
import { changePage } from 'store/actions/pagination'
import { css } from 'emotion'

const pageButtonCSS = css`
  list-style-type: none;
  text-align: center;
  padding: 0;
  margin-top: 40px;
  margin-bottom: 40px;
  li {
    display: inline-block;
    padding: 16px;
  }
  .rc-pagination-disabled,
  .rc-pagination-item-active {
    color: #b7b7b7;
  }
`

const Pagination = ({ currentIndex, total, setPage }) =>
  !!total && (
    <ReactPagination
      className={pageButtonCSS}
      onChange={setPage}
      current={currentIndex}
      total={total}
      prevIcon={<span>Prev</span>}
      nextIcon={<span>Next</span>}
      showPrevNextJumpers={false}
      pageSize={10}
    />
  )

const mapStateToProps = state => ({
  currentIndex: state.pagination,
})

const mapDispatchToProps = dispatch => ({
  setPage: index => dispatch(changePage(index)),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Pagination)

Pagination.propTypes = {
  currentIndex: PropTypes.number.isRequired,
  total: PropTypes.number.isRequired,
  setPage: PropTypes.func.isRequired,
}
