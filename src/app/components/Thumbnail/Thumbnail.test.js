import React from 'react'
import Thumbnail from './index'

it('should render default', () => {
  const wrapper = renderer.create(<Thumbnail />)
  expect(wrapper.toJSON()).toMatchSnapshot()
})

it('should render with image', () => {
  const wrapper = renderer.create(<Thumbnail img="foo.jpg" />)
  expect(wrapper.toJSON()).toMatchSnapshot()
})

it('should render with title', () => {
  const wrapper = renderer.create(<Thumbnail img="foo.jpg" title="bar" />)
  expect(wrapper.toJSON()).toMatchSnapshot()
})
