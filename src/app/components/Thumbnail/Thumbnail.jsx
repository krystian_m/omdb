import React from 'react'
import PropTypes from 'prop-types'
import { css, cx } from 'emotion'

const thumbnailCSS = css`
  max-width: 300px;
  margin-bottom: 24px;
  border: solid 1px #dedede;
  box-shadow: 0px 1px 6px grey;
`
const imgCSS = css`
  width: 100%;
  vertical-align: middle;
`

const DetailThumbnail = ({ className, title, img }) =>
  !!img && (
    <div className={cx(className, thumbnailCSS)}>
      <img src={img} alt={title} className={imgCSS} />
    </div>
  )

export default DetailThumbnail

DetailThumbnail.propTypes = {
  className: PropTypes.string,
  title: PropTypes.string,
  img: PropTypes.string,
}

DetailThumbnail.defaultProps = {
  className: '',
  title: '',
  img: '',
}
