import React from 'react'
import Error from './index'

it('should render default', () => {
  const wrapper = renderer.create(<Error />)
  expect(wrapper.toJSON()).toMatchSnapshot()
})

it('should render with text', () => {
  const wrapper = renderer.create(<Error text="Foo" />)
  expect(wrapper.toJSON()).toMatchSnapshot()
})
