import React from 'react'
import PropTypes from 'prop-types'
import { css } from 'emotion'

const errorCSS = css`
  color: red;
  margin-top: 24px;
  margin-bottom: 24px;
`

const Error = ({ text }) =>
  !!text && <div className={errorCSS}>Error: {text}</div>

export default Error

Error.propTypes = {
  text: PropTypes.string,
}

Error.defaultProps = {
  error: '',
}
