import React from 'react'
import PropTypes from 'prop-types'
import { Helmet } from 'react-helmet'
import { connect } from 'react-redux'
import { fetch } from 'store/actions/fetch'
import { css } from 'emotion'
import DetailHeader from 'components/DetailHeader'
import DetailFooter from 'components/DetailFooter'
import DetailField from 'components/DetailField'
import Thumbnail from 'components/Thumbnail'
import Loading from 'components/Loading'
import Error from 'components/Error'

const detailCSS = css`
  position: relative;
  font-size: 0.875rem;
  font-weight: 300;
  line-height: 1.4;
`
const containerCSS = css`
  display: flex;
  align-items: flex-start;
  flex-direction: column;
  @media (min-width: 768px) {
    flex-direction: row;
  }
`
const contentCSS = css`
  @media (min-width: 768px) {
    margin-left: 40px;
    max-width: 700px;
  }
`
const thumbnailCSS = css`
  flex-shrink: 0;
`

class Detail extends React.PureComponent {
  componentDidMount() {
    const { fetchData, match } = this.props
    fetchData({ id: match.params[0] })
  }

  render() {
    const {
      error,
      loading,
      movie: {
        poster,
        title,
        plot,
        runtime,
        website,
        genre,
        year,
        country,
        writer,
        director,
        production,
        awards,
        actors,
        imdbRating,
        imdbVotes,
      },
    } = this.props
    return (
      <article className={detailCSS}>
        <Helmet title={title} />
        <Error text={error} />

        <div className={containerCSS}>
          <Thumbnail className={thumbnailCSS} title={title} img={poster} />
          <div className={contentCSS}>
            <DetailHeader
              title={title}
              details={[year, genre, country, runtime]}
            />

            <DetailField>{plot}</DetailField>
            <DetailField title="Writen by">{writer}</DetailField>
            <DetailField title="Directed by">{director}</DetailField>
            <DetailField title="Produced by">{production}</DetailField>
            <DetailField title="Produced by">{production}</DetailField>
            <DetailField title="Awards">{awards}</DetailField>
            <DetailField title="Starring">{actors}</DetailField>
            <DetailField title="Rating" condition={!!imdbRating}>
              {imdbRating}
              {!!imdbVotes && <span> ({imdbVotes} votes)</span>}
            </DetailField>

            <DetailFooter website={website} />
          </div>
        </div>

        <Loading show={loading} />
      </article>
    )
  }
}

const mapStateToProps = ({ movies }) => ({
  movie: movies.detailResult,
  loading: movies.loading,
  error: movies.error,
})

const mapDispatchToProps = dispatch => ({
  fetchData: props => dispatch(fetch(props)),
})

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Detail)

Detail.propTypes = {
  // eslint-disable-next-line react/forbid-prop-types
  match: PropTypes.object.isRequired,
  fetchData: PropTypes.func.isRequired,
  loading: PropTypes.bool,
  error: PropTypes.string,
  movie: PropTypes.shape({
    actors: PropTypes.string,
    awards: PropTypes.string,
    country: PropTypes.string,
    director: PropTypes.string,
    genre: PropTypes.string,
    plot: PropTypes.string,
    poster: PropTypes.string,
    production: PropTypes.string,
    runtime: PropTypes.string,
    title: PropTypes.string,
    website: PropTypes.string,
    writer: PropTypes.string,
    year: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
    imdbRating: PropTypes.number,
    imdbVotes: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
  }),
}

Detail.defaultProps = {
  movie: {},
  loading: false,
  error: '',
}
