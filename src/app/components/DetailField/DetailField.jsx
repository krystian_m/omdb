import React from 'react'
import PropTypes from 'prop-types'
import { css } from 'emotion'

const detailFieldCSS = css`
  margin-top: 16px;
  margin-bottom: 16px;
`
const titleCSS = css`
  font-weight: 700;
`
const itemCSS = css`
  margin-right: 16px;
`

// eslint-disable-next-line react/prop-types
const RenderChildren = ({ children }) => {
  if (typeof children !== 'string') return children
  const arr = children.split(/, ?/)
  if (arr.length <= 1) return children

  return (
    <React.Fragment>
      <br />
      {arr.map(x => (
        <span key={x} className={itemCSS}>
          {x}
        </span>
      ))}
    </React.Fragment>
  )
}

const DetailField = ({ title, children, condition }) =>
  !!condition &&
  !!children && (
    <div className={detailFieldCSS}>
      {title && <span className={titleCSS}>{title}: </span>}
      <RenderChildren>{children}</RenderChildren>
    </div>
  )

export default DetailField

DetailField.propTypes = {
  title: PropTypes.string,
  children: PropTypes.node,
  condition: PropTypes.bool,
}

DetailField.defaultProps = {
  title: '',
  children: null,
  condition: true,
}
