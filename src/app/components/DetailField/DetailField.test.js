import React from 'react'
import DetailField from './index'

it('should render default', () => {
  const wrapper = renderer.create(<DetailField />)
  expect(wrapper.toJSON()).toMatchSnapshot()
})

it('should render with children as simple string', () => {
  const wrapper = renderer.create(<DetailField>Foo Bar</DetailField>)
  expect(wrapper.toJSON()).toMatchSnapshot()
})

it('should render with children as string with comma', () => {
  const wrapper = renderer.create(<DetailField>Foo, Bar</DetailField>)
  expect(wrapper.toJSON()).toMatchSnapshot()
})

it('should render with children as node', () => {
  const wrapper = renderer.create(
    <DetailField>
      <div>Foo</div>
    </DetailField>
  )
  expect(wrapper.toJSON()).toMatchSnapshot()
})

it('should renderwith condition', () => {
  const wrapper = renderer.create(
    <DetailField condition={false}>Foo</DetailField>
  )
  expect(wrapper.toJSON()).toMatchSnapshot()
})
