import React from 'react'
import PropTypes from 'prop-types'
import { Link } from 'react-router-dom'
import { css } from 'emotion'
import Thumbnail from 'components/Thumbnail'
import Caption from 'components/Caption'

const movieCSS = css`
  display: inline-block;
  width: 100%;
  margin-bottom: 24px;
  @media (min-width: 576px) {
    margin-bottom: 64px;
  }
`
const linkCSS = css`
  width: 100%;
  text-decoration: none;
  color: inherit;
  display: flex;
  @media (min-width: 576px) {
    flex-direction: column;
  }
`
const thumbnailCSS = css`
  flex-shrink: 0;
  width: 30%;
  @media (min-width: 576px) {
    width: auto;
  }
`
const contentCSS = css`
  margin-left: 16px;
  @media (min-width: 576px) {
    margin-left: 0;
  }
`
const titleCSS = css`
  font-size: 1.125rem;
  line-height: 1.5;
  margin-bottom: 4px;
`

const Movie = ({ imdbID, title, year, type, poster }) => (
  <article className={movieCSS}>
    <Link to={`/${imdbID}`} className={linkCSS}>
      <Thumbnail className={thumbnailCSS} img={poster} title={title} />
      <div className={contentCSS}>
        <div className={titleCSS}>{title}</div>
        <Caption details={[year, type]} />
      </div>
    </Link>
  </article>
)

export default Movie

Movie.propTypes = {
  imdbID: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  year: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
  type: PropTypes.string,
  poster: PropTypes.string,
}

Movie.defaultProps = {
  poster: '',
  year: 0,
  type: '',
}
