import React from 'react'
import { MemoryRouter } from 'react-router'
import Movie from './index'

const withRouter = child => (
  <MemoryRouter initialEntries={['/']}>{child}</MemoryRouter>
)

it('should render with default', () => {
  const wrapper = renderer.create(
    withRouter(<Movie imdbID="foo" title="bar" />)
  )
  expect(wrapper.toJSON()).toMatchSnapshot()
})

it('should render with year', () => {
  const wrapper = renderer.create(
    withRouter(<Movie imdbID="foo" title="bar" year="1999" />)
  )
  expect(wrapper.toJSON()).toMatchSnapshot()
})

it('should render with type', () => {
  const wrapper = renderer.create(
    withRouter(<Movie imdbID="foo" title="bar" type="buzz" />)
  )
  expect(wrapper.toJSON()).toMatchSnapshot()
})

it('should render with poster', () => {
  const wrapper = renderer.create(
    withRouter(<Movie imdbID="foo" title="bar" poster="buzz" />)
  )
  expect(wrapper.toJSON()).toMatchSnapshot()
})
