import React from 'react'
import { MemoryRouter } from 'react-router'
import Movies from './index'

const withRouter = child => (
  <MemoryRouter initialEntries={['/']}>{child}</MemoryRouter>
)

const items = [
  {
    imdbID: '1',
    title: 'a',
  },
  {
    imdbID: '2',
    title: 'b',
  },
]

it('should render with default', () => {
  const wrapper = renderer.create(withRouter(<Movies items={items} />))
  expect(wrapper.toJSON()).toMatchSnapshot()
})
