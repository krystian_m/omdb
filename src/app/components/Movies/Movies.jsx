import React from 'react'
import PropTypes from 'prop-types'
import { css } from 'emotion'
import Movie from 'components/Movie'
import Loading from 'components/Loading'

const moviesCSS = css`
  position: relative;
  @media (min-width: 576px) {
    columns: 2;
    column-gap: 5%;
  }
  @media (min-width: 768px) {
    columns: 0;
    column-width: 200px;
  }
`

const Movies = ({ items, loading }) => (
  <div className={moviesCSS}>
    {items.map(data => (
      <Movie key={data.imdbID} {...data} />
    ))}
    <Loading show={loading} />
  </div>
)

export default Movies

Movies.propTypes = {
  items: PropTypes.arrayOf(
    PropTypes.shape({
      imdbID: PropTypes.string.isRequired,
      title: PropTypes.string.isRequired,
      year: PropTypes.oneOfType([PropTypes.number, PropTypes.string]),
      type: PropTypes.string,
      poster: PropTypes.string,
    })
  ),
  loading: PropTypes.bool,
}

Movies.defaultProps = {
  items: [],
  loading: false,
}
