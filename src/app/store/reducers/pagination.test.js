import reducer from './pagination'

it('should reduce CHANGE_PAGE', () => {
  expect(reducer(null, { type: 'CHANGE_PAGE', index: 3 })).toBe(3)
})

it('should reduce RESET_PAGE', () => {
  expect(reducer(null, { type: 'RESET_PAGE' })).toBe(1)
})

it('should return store as default', () => {
  expect(reducer(3, { type: '_NONE_' })).toBe(3)
})
