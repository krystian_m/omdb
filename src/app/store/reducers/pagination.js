import { CHANGE_PAGE, RESET_PAGE } from 'store/actions/pagination'

const initialState = 1

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case CHANGE_PAGE: {
      return action.index
    }
    case RESET_PAGE: {
      return initialState
    }
    default:
      return state
  }
}
