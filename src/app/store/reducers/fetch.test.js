import reducer, { initialState } from './fetch'

it('should reduce CHANGE_PAGE detail', () => {
  expect(
    reducer(undefined, { type: 'FETCH_DONE', data: { a: 1 }, param: 'foo' })
  ).toEqual({
    ...initialState,
    detailResult: { a: 1 },
    cache: {
      foo: { a: 1 },
    },
  })
})

it('should reduce CHANGE_PAGE detail without param', () => {
  expect(reducer(undefined, { type: 'FETCH_DONE', data: { a: 1 } })).toEqual({
    ...initialState,
    detailResult: { a: 1 },
  })
})

it('should reduce CHANGE_PAGE search', () => {
  expect(
    reducer(undefined, {
      type: 'FETCH_DONE',
      data: { search: [{ a: 1 }] },
      param: 'foo',
    })
  ).toEqual({
    ...initialState,
    searchResult: {
      search: [{ a: 1 }],
    },
    cache: {
      foo: { search: [{ a: 1 }] },
    },
  })
})

it('should reduce FETCH_DETAIL_RESET', () => {
  expect(reducer(undefined, { type: 'FETCH_DETAIL_RESET' })).toEqual(
    initialState
  )
})

it('should reduce FETCH_ERROR', () => {
  expect(reducer(undefined, { type: 'FETCH_ERROR', error: 'foo' })).toEqual({
    ...initialState,
    error: 'foo',
  })
})

it('should reduce FETCH_ERROR_RESET', () => {
  expect(reducer(undefined, { type: 'FETCH_ERROR_RESET' })).toEqual({
    ...initialState,
    error: '',
  })
})

it('should reduce FETCH_LOADING_START', () => {
  expect(reducer(undefined, { type: 'FETCH_LOADING_START' })).toEqual({
    ...initialState,
    loading: true,
  })
})

it('should reduce FETCH_LOADING_FINISH', () => {
  expect(reducer(undefined, { type: 'FETCH_LOADING_FINISH' })).toEqual({
    ...initialState,
    loading: false,
  })
})
