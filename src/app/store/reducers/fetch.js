import {
  FETCH_DONE,
  FETCH_ERROR,
  FETCH_ERROR_RESET,
  FETCH_LOADING_START,
  FETCH_LOADING_FINISH,
  FETCH_DETAIL_RESET,
} from 'store/actions/fetch'

export const initialState = {
  searchResult: {
    search: [],
    totalResults: 0,
  },
  detailResult: {},
  cache: {},
  error: '',
  loading: false,
}

export default (state = initialState, action = {}) => {
  switch (action.type) {
    case FETCH_DONE: {
      const { param, data } = action
      return {
        ...state,
        cache: {
          ...state.cache,
          ...(param ? { [param]: data } : {}),
        },
        detailResult: data.search ? state.detailResult : data,
        searchResult: data.search ? data : state.searchResult,
      }
    }
    case FETCH_DETAIL_RESET: {
      return {
        ...state,
        detailResult: initialState.detailResult,
      }
    }
    case FETCH_ERROR: {
      return {
        ...state,
        error: action.error,
      }
    }
    case FETCH_ERROR_RESET: {
      return {
        ...state,
        error: initialState.error,
      }
    }
    case FETCH_LOADING_START: {
      return {
        ...state,
        loading: true,
      }
    }
    case FETCH_LOADING_FINISH: {
      return {
        ...state,
        loading: false,
      }
    }
    default:
      return state
  }
}
