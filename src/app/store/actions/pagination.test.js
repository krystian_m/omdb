import {
  changePage,
  resetPage,
  CHANGE_PAGE,
  RESET_PAGE,
} from 'store/actions/pagination'

it('should create an action to change a page', () => {
  expect(changePage(3)).toEqual({ type: CHANGE_PAGE, index: 3 })
})

it('should create an action to reset a page to initial value', () => {
  expect(resetPage()).toEqual({ type: RESET_PAGE })
})
