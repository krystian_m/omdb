export const FETCH = 'FETCH'
export const FETCH_DONE = 'FETCH_DONE'
export const FETCH_ERROR = 'FETCH_ERROR'
export const FETCH_CANCEL = 'FETCH_CANCEL'
export const FETCH_ERROR_RESET = 'FETCH_ERROR_RESET'
export const FETCH_LOADING_START = 'FETCH_LOADING_START'
export const FETCH_LOADING_FINISH = 'FETCH_LOADING_FINISH'
export const FETCH_DATA_REQUESTED = 'FETCH_DATA_REQUESTED'
export const FETCH_DETAIL_RESET = 'FETCH_DETAIL_RESET'

// args { id, title, search, type, year }
export const fetch = args => ({ type: FETCH, args })

export const fetchDone = (data, param) => ({ type: FETCH_DONE, data, param })

export const fetchError = error => ({ type: FETCH_ERROR, error })

export const fetchCancel = () => ({ type: FETCH_CANCEL })

export const fetchErrorReset = () => ({ type: FETCH_ERROR_RESET })

export const fetchLoadingStart = () => ({ type: FETCH_LOADING_START })

export const fetchLoadingFinish = () => ({ type: FETCH_LOADING_FINISH })

export const fetchDataRequest = param => ({ type: FETCH_DATA_REQUESTED, param })

export const fetchDetailReset = () => ({ type: FETCH_DETAIL_RESET })
