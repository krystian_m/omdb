export const CHANGE_PAGE = 'CHANGE_PAGE'
export const RESET_PAGE = 'RESET_PAGE'

export const changePage = index => ({
  type: CHANGE_PAGE,
  index,
})

export const resetPage = () => ({ type: RESET_PAGE })
