import {
  fetch,
  fetchDone,
  fetchError,
  fetchCancel,
  fetchErrorReset,
  fetchDetailReset,
  fetchLoadingStart,
  fetchLoadingFinish,
  fetchDataRequest,
  FETCH,
  FETCH_DONE,
  FETCH_ERROR,
  FETCH_CANCEL,
  FETCH_LOADING_START,
  FETCH_LOADING_FINISH,
  FETCH_ERROR_RESET,
  FETCH_DATA_REQUESTED,
  FETCH_DETAIL_RESET,
} from 'store/actions/fetch'

it('should create an action to start fetch data', () => {
  expect(fetch(3)).toEqual({ type: FETCH, args: 3 })
})

it('should create an action to apply data from fetch', () => {
  expect(fetchDone(3, 4)).toEqual({ type: FETCH_DONE, data: 3, param: 4 })
})

it('should create an action to apply fetch error', () => {
  expect(fetchError(3)).toEqual({ type: FETCH_ERROR, error: 3 })
})

it('should create an action to cancel fetch', () => {
  expect(fetchCancel()).toEqual({ type: FETCH_CANCEL })
})

it('should create an action to remove error message', () => {
  expect(fetchErrorReset()).toEqual({ type: FETCH_ERROR_RESET })
})

it('should create an action to remove detail data', () => {
  expect(fetchDetailReset()).toEqual({ type: FETCH_DETAIL_RESET })
})

it('should create an action to start fetch loading', () => {
  expect(fetchLoadingStart()).toEqual({ type: FETCH_LOADING_START })
})

it('should create an action to finish fetch loading', () => {
  expect(fetchLoadingFinish()).toEqual({ type: FETCH_LOADING_FINISH })
})

it('should create an action to send request', () => {
  expect(fetchDataRequest(3)).toEqual({ type: FETCH_DATA_REQUESTED, param: 3 })
})
