import fetch from 'store/reducers/fetch'
import pagination from 'store/reducers/pagination'
import {
  dataRequestEpic,
  searchEpic,
  pageEpic,
  fetchEpic,
} from 'store/epics/fetch'

export const reducers = {
  movies: fetch,
  pagination,
}

export const epics = [dataRequestEpic(), searchEpic, pageEpic, fetchEpic]
