import { pipe } from 'rxjs'
import { map, tap } from 'rxjs/operators'
import jsonNormalize from '../../../utils/jsonNormalize'

const UNKNOWN_ERROR = 'unknown data error'

const protocol = (process.env.BROWSER && window.location.protocol) || 'https:'
const OMDB_URL = `${protocol}//www.omdbapi.com`
const OMDB_KEY = '81e431a4'

export const createUrl = (param, key) =>
  `${OMDB_URL}/?${param}&apikey=${key || OMDB_KEY}`

export const urlArgs = pipe(
  map(({ id, title, search, type, year, page }) => {
    /* eslint-disable prettier/prettier, no-nested-ternary */
    const firstArg =
      id ? `i=${id.trim()}` : '' ||
      title ? `t=${title.trim()}` : '' ||
      search ? `s=${search.trim()}` : ''
    /* eslint-enabled prettier/prettier, no-nested-ternary */
    let out = firstArg
    out += type ? `&type=${type}` : ''
    out += year ? `&y=${year}` : ''
    out += page ? `&page=${page}` : ''
    return out
  })
)

export const scrollRestoration = pipe(
  tap(() => process.browser && window.scrollTo(0, 0))
)

export const dataNormalize = pipe(map(x => jsonNormalize(x)))

export const checkData = callback => pipe(
  map(
    action => action.data.response
      ? action
      : callback(action.data.error || UNKNOWN_ERROR)
  )
)

export const createParamObj = state$ => pipe(
  map(args => {
    if (args) return args
    const { form, pagination } = state$.value
    const values = form && form.search && form.search.values
    return { page: pagination, ...(values || {}) }
  })
)
