/* eslint-disable prettier/prettier */
import { of } from 'rxjs'
import { map } from 'rxjs/operators'
import { marbles } from 'rxjs-marbles/jest'
import printMarbles from 'rxjs-print-marbles'
import { resetPage, changePage, CHANGE_PAGE } from 'store/actions/pagination'
import { fetch, fetchDone, fetchError, fetchDataRequest, fetchLoadingFinish  } from 'store/actions/fetch'
import { dataRequestEpic, searchEpic, pageEpic, fetchEpic } from './fetch'
import { createUrl } from './fetchUtils'

// https://github.com/cartant/rxjs-marbles/blob/0832f9e0994f801b4a9aef580b8353789ae63cfb/examples/jest/basic-spec.ts

it('should request server', marbles(m => {

  const input = 'i=foo'
  const output = ''

  const action = fetchDataRequest(input)
  const completion = fetchLoadingFinish() // fetchDone(output)

  const source =  m.hot("--^-a-|", { a: action })
  const subs =            "^---!"
  const expected = m.cold("--x-|", { x: fetchLoadingFinish(), y: fetchError('unknown data error') })

  // global.window = new JSDOM('', { url: 'https://www.omdbapi.com' })
  const destination = dataRequestEpic({ getJSON: () => of({ response: true, foo: 2 }) })(source)
  m.expect(destination).toBeObservable(expected)
  // m.expect(source).toHaveSubscriptions(subs)
}))

it('should support marble tests with values', marbles(m => {

  const inputs = 1
  // const outputs = { x: 2, y: 3, z: 4 }

  const action = changePage(inputs)
  const completion = fetch()

  const source =  m.hot("--^-a-|", { a: action })
  const subs =            "^---!"
  const expected = m.cold("--x-|", { x: completion })
  // const response = m.cold("--r-|", { r: inputs })

  const destination = pageEpic(source)
  m.expect(destination).toBeObservable(expected)
  m.expect(source).toHaveSubscriptions(subs)
}))
