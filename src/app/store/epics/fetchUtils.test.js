/* eslint-disable prettier/prettier */
import { marbles } from 'rxjs-marbles/jest'
import {
  urlArgs,
  scrollRestoration,
  dataNormalize,
  checkData,
  createParamObj,
  createUrl,
} from './fetchUtils'

it('create server url with param', () => {
  expect(createUrl('foo', 'bar')).toBe('https://www.omdbapi.com/?foo&apikey=bar')
})

describe('urlArgs()', () => {
  it('should support marble tests with id', marbles(m => {

    const inputs = { id: 'foo' }
    const outputs = 'i=foo'

    const source =   m.cold("-a|", { a: inputs })
    const sub =             "^-!"
    const expected = m.cold("-x|", { x: outputs })

    const destination = source.pipe(urlArgs)
    m.expect(destination).toBeObservable(expected)
    m.expect(source).toHaveSubscriptions(sub)
  }))

  it('should support marble tests with title', marbles(m => {

    const inputs = { title: 'foo' }
    const outputs = 't=foo'

    const source =   m.cold("-a|", { a: inputs })
    const sub =             "^-!"
    const expected = m.cold("-x|", { x: outputs })

    const destination = source.pipe(urlArgs)
    m.expect(destination).toBeObservable(expected)
    m.expect(source).toHaveSubscriptions(sub)
  }))

  it('should support marble tests with search, type, year, page', marbles(m => {

    const inputs = { search: 'foo', type: 'bar', year: '19', page: 3 }
    const outputs = 's=foo&type=bar&y=19&page=3'

    const source =   m.cold("-a|", { a: inputs })
    const sub =             "^-!"
    const expected = m.cold("-x|", { x: outputs })

    const destination = source.pipe(urlArgs)
    m.expect(destination).toBeObservable(expected)
    m.expect(source).toHaveSubscriptions(sub)
  }))

  it('should support marble tests witout input', marbles(m => {

    const inputs = {}
    const outputs = ''

    const source =   m.cold("-a|", { a: inputs })
    const sub =             "^-!"
    const expected = m.cold("-x|", { x: outputs })

    const destination = source.pipe(urlArgs)
    m.expect(destination).toBeObservable(expected)
    m.expect(source).toHaveSubscriptions(sub)
  }))
})

describe('scrollRestoration()', () => {
  it('should work in browser', marbles(m => {

    const source =   m.cold("-a|")
    const sub =             "^-!"
    const expected = m.cold("-a|")

    global.window = { scrollTo: () => {} }
    global.process = { browser: true }
    const destination = source.pipe(scrollRestoration)
    m.expect(destination).toBeObservable(expected)
    m.expect(source).toHaveSubscriptions(sub)
  }))

  it('should work in ssr mode', marbles(m => {

    const source =   m.cold("-a|")
    const sub =             "^-!"
    const expected = m.cold("-a|")

    global.process = { browser: false }
    const destination = source.pipe(scrollRestoration)
    m.expect(destination).toBeObservable(expected)
    m.expect(source).toHaveSubscriptions(sub)
  }))
})

it('should dataNormalize operator work', marbles(m => {

  const input = { BigTitle: '3' }
  const output = { bigTitle: 3 }

  const source =   m.cold('-a|', { a: input })
  const sub =             '^-!'
  const expected = m.cold('-x|', { x: output })

  const destination = source.pipe(dataNormalize)
  m.expect(destination).toBeObservable(expected)
  m.expect(source).toHaveSubscriptions(sub)
}))

describe('checkData()', () => {
  it('should return input', marbles(m => {

    const input = { data: { response: true } }

    const source =   m.cold('-a|', { a: input })
    const sub =             '^-!'
    const expected = m.cold('-x|', { x: input })

    const destination = source.pipe(checkData())
    m.expect(destination).toBeObservable(expected)
    m.expect(source).toHaveSubscriptions(sub)
  }))

  it('should run callback when response error', marbles(m => {

    const input = { data: { response: false, error: '_error_text_' } }
    const ouput = '_error_text_'

    const source =   m.cold('-a|', { a: input })
    const sub =             '^-!'
    const expected = m.cold('-x|', { x: ouput })

    const destination = source.pipe(checkData(err => err))
    m.expect(destination).toBeObservable(expected)
    m.expect(source).toHaveSubscriptions(sub)
  }))
  it('should run callback when response error and return unknown error', marbles(m => {

    const input = { data: { response: false } }
    const ouput = 'unknown data error'

    const source =   m.cold('-a|', { a: input })
    const sub =             '^-!'
    const expected = m.cold('-x|', { x: ouput })

    const destination = source.pipe(checkData(err => err))
    m.expect(destination).toBeObservable(expected)
    m.expect(source).toHaveSubscriptions(sub)
  }))
})

describe('createParamObj()', () => {
  it('should return input', marbles(m => {

    const input = { id: 'foo' }

    const source =   m.cold('-a|', { a: input })
    const sub =             '^-!'
    const expected = m.cold('-x|', { x: input })

    const destination = source.pipe(createParamObj())
    m.expect(destination).toBeObservable(expected)
    m.expect(source).toHaveSubscriptions(sub)
  }))

  it('should return values from store', marbles(m => {

    const store = { value: { form: { search: { values: { search: 'foo' } } }, pagination: 2 } }
    const output = { search: 'foo', page: 2 }

    const source =   m.cold('-a|', { a: undefined })
    const sub =             '^-!'
    const expected = m.cold('-x|', { x: output })

    const destination = source.pipe(createParamObj(store))
    m.expect(destination).toBeObservable(expected)
    m.expect(source).toHaveSubscriptions(sub)
  }))

  it('should return when form is empty', marbles(m => {

    const store = { value: { form: { search: {} }, pagination: 2 } }
    const output = { page: 2 }

    const source =   m.cold('-a|', { a: undefined })
    const sub =             '^-!'
    const expected = m.cold('-x|', { x: output })

    const destination = source.pipe(createParamObj(store))
    m.expect(destination).toBeObservable(expected)
    m.expect(source).toHaveSubscriptions(sub)
  }))
})

