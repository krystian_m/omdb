import { actionTypes as reduxFormActions } from 'redux-form'
import { ofType } from 'redux-observable'
import { of } from 'rxjs'
import { ajax } from 'rxjs/ajax'
import {
  mergeMap,
  pluck,
  map,
  filter,
  catchError,
  debounceTime,
  takeUntil,
} from 'rxjs/operators'
import { resetPage, CHANGE_PAGE } from 'store/actions/pagination'
import {
  fetch,
  fetchDone,
  fetchError,
  fetchErrorReset,
  fetchDetailReset,
  fetchLoadingStart,
  fetchLoadingFinish,
  fetchDataRequest,
  FETCH,
  FETCH_CANCEL,
  FETCH_DATA_REQUESTED,
} from 'store/actions/fetch'
import {
  urlArgs,
  scrollRestoration,
  dataNormalize,
  checkData,
  createParamObj,
  createUrl,
} from './fetchUtils'

const REDUX_FORM_CHANGE = reduxFormActions.CHANGE

export const dataRequestEpic = (request = ajax) => action$ =>
  action$.pipe(
    ofType(FETCH_DATA_REQUESTED),
    mergeMap(({ param }) =>
      request.getJSON(createUrl(param)).pipe(
        dataNormalize,
        map(response => fetchDone(response, param)),
        checkData(error => fetchError(error)),
        catchError(error => fetchError(error)),
        takeUntil(action$.pipe(ofType(FETCH_CANCEL)))
      )
    ),
    mergeMap(action => of(fetchLoadingFinish(), action))
  )

export const searchEpic = (action$, state$) =>
  action$.pipe(
    ofType(REDUX_FORM_CHANGE),
    filter(({ meta }) => meta.form === 'search'),
    filter(() => {
      const { values } = state$.value.form.search
      return values && values.search && values.search.trim().length >= 3
    }),
    debounceTime(200),
    mergeMap(
      () =>
        state$.value.pagination
          ? of(resetPage(), fetchErrorReset(), fetch())
          : of(fetchErrorReset(), fetch())
    )
  )

export const pageEpic = action$ =>
  action$.pipe(
    ofType(CHANGE_PAGE),
    map(() => fetch()),
    scrollRestoration
  )

// FIX: Find better solution for detail reset / first data.
export const fetchEpic = (action$, state$) =>
  action$.pipe(
    ofType(FETCH),
    pluck('args'),
    createParamObj(state$),
    urlArgs,
    mergeMap(param => {
      const { cache } = state$.value.movies
      return cache[param]
        ? of(fetchDone(cache[param]))
        : of(fetchLoadingStart(), fetchDetailReset(), fetchDataRequest(param))
    })
  )
