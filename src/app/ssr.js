import React from 'react'
import ReactDOM from 'react-dom/server'
import serialize from 'serialize-javascript'
import { extractCritical } from 'emotion-server'
import { Helmet } from 'react-helmet'
import urljoin from 'url-join'
import { Provider as StoreProvider } from 'react-redux'
import App from 'components/App'
import { StaticRouter } from 'react-router'
import createReduxStore from 'create/reduxStore'
import isPlainObject from 'lodash/isPlainObject'
import performancePrint from '../utils/performancePrint'

const debug = require('debug')('ssr')

function normalizeAssets(assets) {
  if (isPlainObject(assets)) {
    return Object.values(assets)
  }
  return Array.isArray(assets) ? assets : [assets]
}

export default ({ clientStats }) => (req, res) => {
  debug('create: client, store, history etc...')
  const { publicPath, assetsByChunkName } = clientStats
  const context = {}
  const initialState = {}
  const store = createReduxStore(initialState)
  debug('create: apollo, redux etc... ok')

  const Component = (
    <StaticRouter location={req.originalUrl} context={context}>
      <StoreProvider store={store}>
        <App />
      </StoreProvider>
    </StaticRouter>
  )
  debug('Component')

  debug('renderToString...')
  const { html: content, ids: emotionIDS, css: criticalCSS } = extractCritical(
    ReactDOM.renderToString(Component)
  )
  debug('renderToString... ok')

  debug('get: state, flush, helmet, chunks...')
  const reduxState = store.getState()
  const language = req.language || 'pl-PL'
  const helmet = Helmet.renderStatic()
  const htmlAttrs = helmet.htmlAttributes.toString()
  const bodyAttrs = helmet.bodyAttributes.toString()
  const data = {
    reduxState,
    emotionIDS,
  }
  debug('get: state, flush, helmet, chunks... ok')

  debug('create html...')
  const html = `<!doctype html>
    <html lang=${language} ${htmlAttrs}>
      <head>
        <meta charset="utf-8">
        ${helmet.title.toString()}
        ${helmet.base.toString()}
        ${helmet.meta.toString()}
        ${helmet.link.toString()}
        ${helmet.style.toString()}
        ${helmet.script.toString()}
        ${normalizeAssets(assetsByChunkName.main)
          .filter(path => path.endsWith('.css'))
          .map(
            path =>
              `<link rel="stylesheet" href="${urljoin(
                '/',
                publicPath,
                path
              )}" />`
          )
          .join('\n')}
        <style>${criticalCSS}</style>
      </head>
      <body ${bodyAttrs}>
        <div id="root">${content}</div>
        ${data &&
          `<script>
            window.App=${serialize(data, {
              isJSON: true, // Initial data for client
            })};
          </script>`}
        ${
          _ANALYZE_
            ? `<script>
                window.onload = function () {
                  ${performancePrint}
                };
              </script>`
            : ''
        }
        ${normalizeAssets(assetsByChunkName.main)
          .filter(path => path.endsWith('.js'))
          .map(
            path => `<script src="${urljoin('/', publicPath, path)}"></script>`
          )
          .join('\n')}
      </body>
    </html>`
  debug('create html... ok')

  res.status(200)
  res.send(html)
}
