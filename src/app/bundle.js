const fs = require('fs')
const webpack = require('webpack')
const clientConfigProd = require('../../config/webpack.client')
const serverConfigProd = require('../../config/webpack.ssr')

const mkdirSync = dirPath => {
  try {
    fs.mkdirSync(dirPath)
  } catch (err) {
    // if (err.code !== 'EEXIST') throw err
  }
}

webpack([clientConfigProd, serverConfigProd]).run((err, stats) => {
  if (err) {
    console.error('Error: ', err)
  }
  const clientStats = stats.toJson().children[0]

  mkdirSync('./build')

  fs.writeFile('./build/stats.json', JSON.stringify(clientStats), error => {
    if (error) {
      return console.error(error)
    }

    console.info('The stats was saved!')
    return null
  })
})
