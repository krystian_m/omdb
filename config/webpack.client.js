/* eslint-disable prettier/prettier, global-require */
require('dotenv-safe').config()
const webpack = require('webpack')
const CompressionPlugin = require('compression-webpack-plugin')
const BrotliPlugin = require('brotli-webpack-plugin')
const { BundleAnalyzerPlugin } = require('webpack-bundle-analyzer')
const webpackMerge = require('webpack-merge')
const { devProdReducer } = require('config-reducer')
const commonConfig = require('./webpack.common')()
const config = require('./index.js')
const paths = require('./paths')

module.exports = webpackMerge(commonConfig, devProdReducer({
  mode: 'development', // TODO: gitlab ci not work
  development: {
    devtool: 'inline-cheap-module-source-map' // 'cheap-module-source-map' // 'inline-source-map'
  },

  name: 'client',
  target: 'web',

  devtool: 'source-map',

  entry: [
    {development: () => [
      require.resolve('react-error-overlay'),
      'webpack-hot-middleware/client?path=/__webpack_hmr&timeout=20000&reload=false&quiet=false&noInfo=false',
      'react-hot-loader/patch'
    ]},
    paths.dir.entry.client,
  ],

  output: {
    development: {
      filename: '[name].js',
      chunkFilename: '[name].js'
    },
    filename: '[name].[chunkhash].js',
    chunkFilename: '[name].[chunkhash].js',
    path: paths.dir.build.public,
    publicPath: paths.url.public,
  },

  // optimization: {
  //     splitChunks: {
  //         cacheGroups: {
  //             default: false,
  //             vendors: false,
  //             // vendor chunk
  //             vendor: {
  //                 // async + async chunks
  //                 chunks: 'all',
  //                 // import file path containing node_modules
  //                 test: /node_modules/
  //             }
  //         }
  //     }
  // },

  plugins: [
    {development: () => [
      new webpack.HotModuleReplacementPlugin(),
      // new webpack.DllReferencePlugin({
      //   context: '.',
      //   manifest: require('../.dll/manifest.json')
      // }),
    ]},
    {production: [
      new CompressionPlugin({
        asset: '[path].gz[query]',
        algorithm: 'gzip',
        test: /\.(js|css|html|ttf|woff|woff2|svg|eot)$/,
        threshold: 8192,
        minRatio: 0.9,
        deleteOriginalAssets: false,
      }),
      new BrotliPlugin({
        asset: '[path].br[query]',
        test: /\.(js|css|html|ttf|woff|woff2|svg|eot)$/,
        threshold: 8192,
        minRatio: 0.9
      }),
    ]},
    ...(config.ANALYZE ? [new BundleAnalyzerPlugin(config.bundleAnalyzer)] : []),
    // {analyze: () => [new BundleAnalyzerPlugin(config.bundleAnalyzer)]}
  ],
}))
