require('dotenv-safe').config()
const path = require('path')
const urlJoin = require('url-join')

const { PWD, PUBLIC_PATH } = process.env
const resolve = (...args) => path.resolve(PWD, path.join(...args))

module.exports = {
  dir: {
    project: PWD,
    src: resolve('src'),
    app: resolve('src', 'app'),
    server: resolve('src', 'server'),
    static: resolve('static'),
    public: resolve('public'),
    assets: resolve('src', 'assets'),
    manifest: resolve('build', 'manifest.json'),
    storybook: resolve('.storybook'),
    storybookHelpers: resolve('.storybook/helpers'),
    modules: resolve('modules'),

    build: {
      path: resolve('build'),
      public: resolve('build', 'public'),
      assets: {
        files: resolve('build', 'public', 'files'),
        videos: resolve('build', 'public', 'videos'),
        images: resolve('build', 'public', 'images'),
      },
    },

    entry: {
      client: resolve('src', 'app', 'client.js'),
      ssr: resolve('src', 'app', 'ssr.js'),
      server: resolve('src/server/index.js'),
    },

    config: {
      postcss: resolve('config', 'postcss.config.js'),
    },

    dll: {
      path: resolve('.dll'),
      filename: 'dll.js',
      manifest: resolve('.dll', 'manifest.json'),
    },

    test: {
      files: resolve('test', 'files'),
    },

    tsconfig: {
      dev: resolve('tsconfig.json'),
      prod: resolve('tsconfig.prod.json'),
    },
    tslint: resolve('tslint.js'),
  },

  url: {
    public: urlJoin('/', PUBLIC_PATH),
    videos: urlJoin('/', PUBLIC_PATH, 'videos'),
    images: urlJoin('/', PUBLIC_PATH, 'images'),
    files: urlJoin('/', PUBLIC_PATH, 'files'),
  },
}
