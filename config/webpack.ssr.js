/* eslint-disable prettier/prettier, global-require */
require('dotenv-safe').config()
const webpack = require('webpack')
// const HtmlWebpackPlugin = require('html-webpack-plugin')
const webpackMerge = require('webpack-merge')
const { devProdReducer } = require('config-reducer')
const commonConfig = require('./webpack.common')(true)
const paths = require('./paths')

module.exports = webpackMerge(commonConfig, devProdReducer({
  mode: 'development', // TODO: gitlab ci not work

  development: () => {
    const nodeExternals = require('webpack-node-externals')
    return {
      devtool: 'inline-cheap-module-source-map', // 'cheap-module-source-map' // 'inline-source-map'
      externals: [nodeExternals({
        whitelist: ['react-loadable'],
      })]
    }
  },

  name: 'server',
  target: 'node',

  devtool: 'none', // 'source-map',

  entry: [
    {development: ['regenerator-runtime/runtime.js']},
    paths.dir.entry.ssr,
  ],

  output: {
    development: { pathinfo: true },
    path: paths.dir.build.path,
    filename: 'ssr.js',
    libraryTarget: 'commonjs2',
  },

  plugins: [
    new webpack.optimize.LimitChunkCountPlugin({ maxChunks: 1 }),
    // new HtmlWebpackPlugin(),
  ],
}))
