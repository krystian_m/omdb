require('dotenv-safe').config()
const webpack = require('webpack')
const Dotenv = require('dotenv-webpack')
const config = require('./index.js')
const paths = require('./paths')

const reScript = /^((?!\.test\.).)*\.js$/

const clientConfig = {
  mode: 'production',

  stats: {
    colors: true,
    warnings: true,
    errors: true,
    errorDetails: true,
    timings: true,
  },

  target: 'node',

  name: 'server',

  devtool: 'eval',

  performance: {
    hints: false,
  },

  entry: paths.dir.entry.server,

  output: {
    path: paths.dir.build.path,
    filename: 'index.js',
    libraryTarget: 'commonjs2',
  },

  module: {
    rules: [
      {
        test: reScript,
        exclude: /(node_modules|build)/,
        loaders: ['babel-loader'],
      },
    ],
  },

  resolve: {
    extensions: ['.js', '.json', '.mjs', '.web.js'],
    modules: [
      paths.dir.server,
      'node_modules',
      paths.dir.assets,
      paths.dir.modules,
    ],
    symlinks: false,
  },

  plugins: [
    new Dotenv(config.dotenv),
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV),
      _DEV_: config.DEV,
      _PROD_: config.PROD,
      _ANALYZE_: config.ANALYZE,
      _TEST_: config.TEST,
    }),
  ],

  // Do not replace node globals with polyfills
  // https://webpack.js.org/configuration/node/
  node: {
    fs: false,
    path: false,
    console: false,
    global: false,
    process: false,
    Buffer: false,
    __filename: false,
    __dirname: false,
  },
}

module.exports = clientConfig
