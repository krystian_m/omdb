/* eslint-disable prettier/prettier, global-require */
require('dotenv-safe').config()
const nodePath = require('path')
const fs = require('fs')
const webpack = require('webpack')
const Dotenv = require('dotenv-webpack')
const CopyWebpackPlugin = require('copy-webpack-plugin')
const { devProdReducer } = require('config-reducer')
const config = require('./index.js')
const paths = require('./paths')
const pkg = require('../package.json')

const fileSize = sizeInBytes => filename => {
  const stats = fs.statSync(filename)
  const fileSizeInBytes = stats.size
  return fileSizeInBytes <= sizeInBytes
}

const reScript = /^((?!\.test|story|spec\.).)*\.(js|jsx)$/
const reStyle = /\.(css|less|styl|scss|sass|sss)$/
const reBitmap = /\.(bmp|gif|jpg|jpeg|png)$/
const reVector = /\.(svg)$/
const reVideo = /\.(mp4|webm)$/

const { PWD } = process.env

module.exports = isSSR => devProdReducer({
  context: PWD,

  development: {
    stats: {
      colors: true,
      warnings: true,
      errors: true,
      errorDetails: true,
      timings: true,
    },
  },

  resolve: {
    extensions: [
      '.js',
      '.jsx',
      '.json',
      '.mjs',
      '.web.js',
      '.web.jsx',
    ],
    modules: [
      paths.dir.app,
      paths.dir.server,
      'node_modules',
      paths.dir.assets,
      paths.dir.modules,
    ],
    symlinks: false,
  },

  module: {
    strictExportPresence: true,
    rules: [
      {
        oneOf: [
          {
            test: reScript,
            include: [paths.dir.src, paths.dir.storybook],
            use: [
              // require.resolve('cache-loader'),
              {
                loader: require.resolve('babel-loader'),
                options: {
                  cacheDirectory: true, // ~ 40% - 300% faster
                  cacheCompression: false,
                  presets: [ // overwrite babel presets
                    [
                      '@babel/preset-env',
                      {
                        targets: isSSR
                          ? { node: pkg.engines.node.replace(/[=><^~]/g, '') }
                          : { browsers: pkg.browserslist },
                      },
                    ],
                    '@babel/preset-react'
                  ],
                }
              },
            ],
          },
          {
            test: reStyle,
            oneOf: [
              {
                include: [paths.dir.app],
                use: [
                  {
                    loader: 'css-loader',
                    options: {
                      // CSS Modules https://github.com/css-modules/css-modules
                      modules: true,
                      localIdentName: '[name]__[local]--[hash:base64:5]',
                      // CSS Nano http://cssnano.co/
                      minimize: { discardComments: { removeAll: true } },
                      development: {
                        minimize: false,
                        sourceMap: true,
                      },
                    }
                  },
                  {
                    loader: 'postcss-loader',
                    options: {
                      config: {
                        path: paths.dir.config.postcss,
                      }
                    }
                  },
                ],
              },
              {
                use: [
                  'css-loader',
                ],
              },
            ],
          },
          {
            test: file => reBitmap.test(file) && fileSize(10000)(file),
            include: [paths.dir.project],
            loader: require.resolve('url-loader'),
          },
          {
            test: reVector,
            include: [paths.dir.project],
            exclude: /(fonts|css)/,
            use: [
              {
                loader: require.resolve('babel-loader'),
                options: {
                  cacheDirectory: true, // ~ 40% - 300% faster
                }
              },
              {
                loader: 'react-svg-loader',
                options: {
                  jsx: true,
                },
              },
            ],
          },

          // Exclude `js` files to keep "css" loader working as it injects
          // its runtime that would otherwise processed through "file" loader.
          // Also exclude `html` and `json` extensions so they get processed
          // by webpacks internal loaders.
          {
            exclude: [/\.(js|jsx|mjs|ejs)$/, /\.html$/, /\.json$/],
            oneOf: [
              {
                include: reBitmap,
                loader: require.resolve('file-loader'),
                options: {
                  name: '[name].[hash:5].[ext]',
                  publicPath: paths.url.public.images,
                  outputPath: 'images',
                  emitFile: !isSSR,
                }
              },
              {
                include: reVideo,
                loader: require.resolve('file-loader'),
                options: {
                  name: '[name].[hash:5].[ext]',
                  publicPath: paths.url.public.videos,
                  outputPath: 'videos',
                  emitFile: !isSSR,
                }
              },
              {
                loader: require.resolve('file-loader'),
                options: {
                  name: '[name].[hash:5].[ext]',
                  publicPath: paths.url.public.files,
                  outputPath: 'files',
                  emitFile: !isSSR,
                }
              },
            ],
          },
        ],
      },
    ],
  },

  plugins: [
    new Dotenv(config.dotenv),
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV),
      _SSR_: true,
      _DEV_: config.DEV,
      _PROD_: config.PROD,
      _ANALYZE_: config.ANALYZE,
      _TEST_: config.TEST,
      _STORYBOOK_: false,
    }),
    new CopyWebpackPlugin([
      {
        from: nodePath.join(paths.dir.public, '**/*'),
        to: paths.dir.build.path,
      }
    ]),

      {development: () => {
        const notifier = require('node-notifier')
        const FriendlyErrorsWebpackPlugin = require('friendly-errors-webpack-plugin')
        const SimpleProgressPlugin = require('webpack-simple-progress-plugin')
        const CaseSensitivePathsPlugin = require('case-sensitive-paths-webpack-plugin')
        // const HardSourceWebpackPlugin = require('hard-source-webpack-plugin')
        // const AutoDllPlugin = require('autodll-webpack-plugin')
        // const WriteFilePlugin = require('write-file-webpack-plugin')

        return [
          new CaseSensitivePathsPlugin(),
          new FriendlyErrorsWebpackPlugin({
            onErrors: (severity, errors) => {
              if (severity !== 'error') {
                return null
              }
              const error = errors[0]
              console.error(`\nerror: ${error.name}\nfile: ${error.file}\n\n${error.webpackError}\n\n`)
              return notifier.notify({
                title: 'Webpack SSR error',
                message: `${severity}: ${error.name}`,
                subtitle: error.file || '',
              })
            }
          }),
          new SimpleProgressPlugin({
            progressOptions: {
              width: 15,
              total: 100,
              clear: false,
            }  ,
          }),
          // new HardSourceWebpackPlugin({
          //   // If the hash is different than a previous build, a fresh cache will be used.
          //   environmentHash: {
          //     root: process.cwd(),
          //     directories: [],
          //     files: ['package-lock.json', 'yarn.lock', '.env']
          //   }
          // }),
          // new WriteFilePlugin(),
          // new AutoDllPlugin({
          //   inject: true, // will inject the DLL bundles to index.html
          //   filename: '[name].js',
          //   entry: {
          //     vendor: [
          //       'react',
          //       'react-dom'
          //     ]
          //   }
          // }),
        ]
      }},
      {production: () => [
        // Remove this if you don't use Moment.js:
        new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
        new webpack.optimize.OccurrenceOrderPlugin(),
        new webpack.HashedModuleIdsPlugin(),

      // wywsala emotion
      // new webpack.optimize.ModuleConcatenationPlugin(), // ------- >  unnecessary ?
    ]},

  ],

  // Some libraries import Node modules but don't use them in the browser.
  // Tell Webpack to provide empty mocks for them so importing them works.
  // https://webpack.js.org/configuration/node/
  // https://github.com/webpack/node-libs-browser/tree/master/mock
  node: {
    dgram: 'empty',
    fs: 'empty',
    net: 'empty',
    tls: 'empty',
    child_process: 'empty',
  },
})
