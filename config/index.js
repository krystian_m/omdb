if (process.env.BROWSER) {
  throw new Error('Do not import `config` from inside the client-side code.')
}

const DEV = process.env.NODE_ENV === 'development'
const PROD = process.env.NODE_ENV === 'production'
const TEST = process.env.NODE_ENV === 'test'
const ANALYZE =
  process.argv.includes('--analyze') || process.argv.includes('--analyse')

// for debuging use cmd 'export DEBUG=...'

module.exports = {
  DEV,
  PROD,
  TEST,
  ANALYZE,

  dotenv: {
    safe: true,
    systemvars: true,
  },

  devMiddleware: publicPath => ({
    // noInfo: true
    // writeToDisk: true,
    publicPath,
    serverSideRender: true,
    watchOptions: {
      aggregateTimeout: 150,
      poll: true,
    },
    stats: {
      errorDetails: true,
      moduleTrace: false,
      publicPath: false,
      warnings: true,
      errors: true,
      timings: true,
      colors: true,
      modules: false,
      hash: false,
      reasons: false,
      source: false,
      performance: false,
      chunks: ANALYZE,
      assets: ANALYZE,
      version: ANALYZE,
      children: ANALYZE, // true - for details

      providedExports: ANALYZE, // Show the exports of the modules
      usedExports: ANALYZE, // Show which exports of a module are used
    },
  }),

  bundleAnalyzer: {
    analyzerHost: 'localhost',
    analyzerPort: 8888,
    // Module sizes to show in report by default.
    // Should be one of `stat`, `parsed` or `gzip`.
    // See "Definitions" section for more information.
    defaultSizes: 'stat',
    // Automatically open report in default browser
    openAnalyzer: true,
    // Log level. Can be 'info', 'warn', 'error' or 'silent'.
    logLevel: 'silent',
  },
}
